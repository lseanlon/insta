var page = require('webpage').create();
var loadInProgress = false;
var testindex = 0;
var system = require('system');

repeatScrollTimes = 0;
if (system.args.length === 1) {
    //-console.log('Usage: google.js <some Query>');
    phantom.exit(1);
} else {
    q = system.args[1];
}


// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

page.onAlert = function(msg) {
    console.log('alert!!> ' + msg);
};

page.onLoadStarted = function() {
    loadInProgress = true;
    console.log("load started");
};

page.onLoadFinished = function(status) {
    loadInProgress = false;
    if (status !== 'success') {
        console.log('Unable to access network');
        phantom.exit();
    } else {
        console.log("load finished");
    }
};

var steps = [
    function() {
        page.open(q);
    },

    function() {
        console.log('Answers1:');
        page.injectJs("jquery1-11-1min.js");
        // page.render('example.png');
        setTimeout(function() {
            page.evaluate(function() {

                jQuery(":contains(Load more)").eq(jQuery(":contains(Load more)").length - 1)[0].click()

            });
        }, 0);
    },
    function() {
        console.log('Answers2:');
        page.injectJs("jquery1-11-1min.js");
        // page.render('example.png');
        setTimeout(function() {

            repeatScrollTimes= repeatScrollTimes + 1;
            page.evaluate(function(repeatScrollTimes) {
                window.document.body.scrollTop = document.body.scrollHeight;
            }, repeatScrollTimes);
        }, 0);
    },
    function() {
        console.log('Answers3:');
        page.injectJs("jquery1-11-1min.js");
        // page.render('example.png');
        setTimeout(function() {
            page.evaluate(function() {

                // console.log('html: ' + jQuery('body').html());
                console.log('html: ' + window.document.body.outerHTML);
  
            });
        }, 0);
    },
    function() {
        console.log('Exiting');
    }
];
interval = setInterval(function() {
    if (!loadInProgress && typeof steps[testindex] == "function") {
        console.log("step " + (testindex + 1));
        steps[testindex]();
 
        if (repeatScrollTimes <= 3) {
            testindex = 2;
        } else {
            testindex++;
        }
    }
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}, 3250);
